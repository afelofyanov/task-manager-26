package ru.tsc.felofyanov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.api.repository.IRepository;
import ru.tsc.felofyanov.tm.model.AbstractModel;

import java.util.*;
import java.util.stream.Collectors;

public class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    protected final List<M> models = new ArrayList<>();

    @NotNull
    @Override
    public List<M> findAll() {
        return models;
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final Comparator<M> comparator) {
        return models.stream()
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public M remove(@NotNull final M model) {
        models.remove(model);
        return model;
    }

    @Override
    public void removeAll(@Nullable final Collection<M> collection) {
        if (collection == null) return;
        models.removeAll(collection);
    }


    @Override
    public void clear() {
        models.clear();
    }

    @NotNull
    @Override
    public M add(@NotNull final M model) {
        models.add(model);
        return model;
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        return models.stream()
                .anyMatch(item -> id.equals(item.getId()));
    }

    @Override
    @Nullable
    public M findOneById(@NotNull final String id) {
        return models.stream()
                .filter(item -> id.equals(item.getId()))
                .findFirst()
                .orElse(null);
    }

    @NotNull
    @Override
    public M findOneByIndex(@NotNull final Integer index) {
        return models.get(index);
    }

    @NotNull
    @Override
    public M removeById(@NotNull final String id) {
        @Nullable final Optional<M> model = Optional.ofNullable(findOneById(id));
        model.ifPresent(models::remove);
        return model.get();
    }

    @NotNull
    @Override
    public M removeByIndex(@NotNull final Integer index) {
        @NotNull final Optional<M> model = Optional.of(findOneByIndex(index));
        model.ifPresent(models::remove);
        return model.get();
    }
}
